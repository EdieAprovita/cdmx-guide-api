/**
/  * @name notFound
/  * @description Middleware to handle 404 errors
  */

const notFound = (req, res, next) => {
	if (!req.originalUrl) {
		res.status(404).json({
			success: false,
			error: 'Url not found',
			message:
				'The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.',
		});
	}
	next();
};

/**
 * @name errorHandler
 * @description Middleware to handle errors
 */

const errorHandler = (err, req, res, next) => {
	const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
	res.status(statusCode);
	res.json({
		message: err.message,
		stack: process.env.NODE_ENV === 'production' ? null : err.stack,
	});
};

export { errorHandler, notFound };
