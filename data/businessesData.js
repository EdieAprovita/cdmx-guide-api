const businesses = [
	{
		name: 'Hierba Buena',
		address: 'Ciudad de México',
		image: 'https://www.facebook.com/HIERBABUENAV/photos/a.331401107037843/1357319984445945/',
		contact: [
			{
				phone: '52 55 2712 6196',

				facebook: 'https://www.facebook.com/HIERBABUENAV',

				instagram: 'https://www.instagram.com/hierba.buenav/',
			},
		],

		budget: '$$',
		typeBusiness: 'Food',
		rating: 4,
		numReviews: 1,
	},
];

export default businesses;
