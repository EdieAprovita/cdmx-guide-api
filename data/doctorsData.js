const doctors = [
	{
		name: 'Saide Mobarak',
		image: 'https://www.facebook.com/photo/?fbid=10159070062715306&set=a.465096485305',
		contact: [
			{
				phone: '55 4145 8055',
				facebook: 'https://www.facebook.com/saide.mobarak',
				instagram: 'https://www.instagram.com/veganfitmx/',
			},
		],

		rating: 4.5,
		numReviews: 4,
	},
];

export default doctors;
