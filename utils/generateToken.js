import jwt from 'jsonwebtoken';
/**
 * @description Generate a new token for the user
 */
const generateToken = id => {
	return jwt.sign({ id }, process.env.JWT_SECRET, {
		expiresIn: '14d',
	});
};

export default generateToken;
