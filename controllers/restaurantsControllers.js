import asyncHandler from 'express-async-handler';
import Restaurant from '../models/Restaurant.js';

/**
 * @description Fetch all restaurants
 * @route GET /api/restaurants
 * @access Public
 * @returns {Object} Restaurants
 */

const getAllRestaurants = asyncHandler(async (req, res) => {
	try {
		const pageSize = 10;
		const page = Number(req.query.pageNumber) || 1;

		const keyword = req.query.pageNumber
			? {
					name: {
						$regex: req.query.keyword,
						$options: 'i',
					},
			  }
			: {};

		const count = await Restaurant.countDocuments({ ...keyword });
		const restaurants = await Restaurant.find({ ...keyword })
			.limit(pageSize)
			.skip(pageSize * (page - 1));

		res.status(200).json({
			message: 'Restaurants fetched successfully',
			restaurants: restaurants,
			page: page,
			pages: Math.ceil(count / pageSize),
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Restaurants',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Fetch single restaurant
 * @route GET /api/restaurants/:id
 * @access Public
 * @returns {Object} Restaurant
 */

const getRestaurant = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const restaurant = await Restaurant.findById(id).populate('User');
		res.status(200).json({
			message: 'Restaurant fetched successfully',
			restaurant: restaurant,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Restaurant',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Create a new restaurant
 * @route POST /api/restaurants/create
 * @access Private
 * @returns {Object} Restaurant
 */

const createRestaurant = asyncHandler(async (req, res) => {
	try {
		const { name, typePlace, address, image, budget, contact, numReviews } = req.body;
		const author = req.user._id;

		const restaurant = await Restaurant.create({
			name: name,
			author: author,
			typePlace: typePlace,
			address: address,
			image: image,
			budget: budget,
			contact: contact,
			numReviews: numReviews,
		});
		res.status(201).json({
			message: 'Restaurant created successfully',
			restaurant: restaurant,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error creating Restaurant',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Update a restaurant
 * @route PUT /api/restaurants/update/:id
 * @access Private
 * @returns {Object} Restaurant
 */

const updateRestaurant = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const { name, typePlace, address, image, budget, contact } = req.body;
		const restaurant = await Restaurant.findByIdAndUpdate(id, {
			name: name,
			typePlace: typePlace,
			address: address,
			image: image,
			budget: budget,
			contact: contact,
		});
		res.status(200).json({
			message: 'Restaurant updated successfully',
			restaurant: restaurant,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error updating Restaurant',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Delete a restaurant
 * @route DELETE /api/restaurants/delete/:id
 * @access Private
 */

const deleteRestaurant = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		await Restaurant.findByIdAndDelete(id);
		res.status(200).json({
			message: 'Restaurant deleted successfully',
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error deleting Restaurant',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Create a new review
 * @route POST /api/restaurants/:id/reviews
 * @access Private
 * @returns {Object} Recipe Review
 */

const createRestaurantReview = asyncHandler(async (req, res) => {
	try {
		const { rating, comment } = req.body;

		const restaurant = await Restaurant.findById(req.params.id);

		if (restaurant) {
			const alreadyReviewed = restaurant.reviews.find(
				r => r.user.toString() === req.user._id.toString()
			);

			if (alreadyReviewed) {
				res.status(500).json({
					message: 'You have already reviewed this restaurant',
					restaurant: restaurant,
					success: false,
				});
			}

			const review = {
				username: req.user.username,
				rating: Number(rating),
				comment: comment,
				user: req.user._id,
			};

			restaurant.reviews.push(review);
			restaurant.numReviews = restaurant.reviews.length;
			restaurant.rating =
				restaurant.reviews.reduce((acc, item) => item.rating + acc, 0) /
				restaurant.reviews.length;

			await restaurant.save();
			res.status(201).json({
				message: 'Review created successfully',
				restaurant: restaurant,
				success: true,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Error creating review',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Get top restaurants
 * @route GET /api/restaurants/top
 * @access Public
 * @returns {Object} Restaurants
 */

const getTopRestaurants = asyncHandler(async (req, res) => {
	try {
		const restaurants = await Restaurant.find({}).sort({ rating: -1 }).limit(3);
		res.status(200).json({
			message: 'Top rated restaurants fetched successfully',
			restaurants: restaurants,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching top rated restaurants',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	getAllRestaurants,
	getRestaurant,
	createRestaurant,
	updateRestaurant,
	deleteRestaurant,
	createRestaurantReview,
	getTopRestaurants,
};
