import asyncHandler from 'express-async-handler';
import normalize from 'normalize';

import Post from '../models/Post.js';
import ProfessionalProfile from '../models/ProfessionProfile.js';
import User from '../models/User.js';

/**
 * @description Fetch all profiles
 * @route GET /api/profiles
 * @access Admin
 */

const getAllProfiles = asyncHandler(async (req, res) => {
	try {
		const pageSize = 10;
		const page = Number(req.query.pageNumber) || 1;

		const keyword = req.query.pageNumber
			? {
					name: {
						$regex: req.query.keyword,
						$options: 'i',
					},
			  }
			: {};

		const count = await ProfessionalProfile.countDocuments({ ...keyword });
		const profiles = await ProfessionalProfile.find({ ...keyword })
			.limit(pageSize)
			.skip(pageSize * (page - 1));

		res.status(200).json({
			message: 'Profiles fetched successfully',
			data: profiles,
			page: page,
			pages: Math.ceil(count / pageSize),
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			success: false,
			error: 'Server Error',
			message: error.message,
		});
	}
});

/**
 * @description Fetch single profile
 * @route GET /api/profiles/:user_id
 * @access Admin
 * @returns {Object} ProfessionalProfile
 */

const getProfile = asyncHandler(async (req, res) => {
	try {
		const profile = await ProfessionalProfile.findOne({
			user: req.user._id,
		}).populate('user', ['name', 'avatar']);

		if (!profile) {
			return res.status(400).json({
				success: false,
				error: 'No profile found',
			});
		}

		res.status(200).json({
			message: 'ProfessionalProfile fetched',
			profile: profile,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			success: false,
			error: 'Server Error',
			message: `${error}`.red,
		});
	}
});

/**
 * @description Create or update user profile
 * @route POST /api/profiles/edit/:user_id
 * @access Private
 * @returns {Object} ProfessionalProfile
 */

const createUpdateProfile = asyncHandler(async (req, res) => {
	try {
		const { contact, skills, experience, education, social } = req.body;

		// Build profile object
		const profileFields = {};

		profileFields.user = req.user._id;

		//Build contact object
		const contactFields = { phone, email };

		if (contact) profileFields.contact = contactFields;

		if (skills) profileFields.skills = skills.split(',').map(skill => skill.trim());

		//Build social object
		const socialFields = { facebook, twitter, linkedin, instagram, youtube };

		//normalize social fields
		for (const [key, value] of Object.entries(socialFields)) {
			if (value && value.length > 0)
				socialFields[key] = normalize(value, { forceHttps: true });
		}

		//add social object to profileFields
		if (social) profileFields.social = socialFields;

		//Build experience object
		const experienceFields = {
			title,
			company,
			location,
			from,
			to,
			current,
			description,
		};

		//add experience object to profileFields
		if (experience) profileFields.experience = experienceFields;

		//Build education object
		const educationFields = {
			school,
			degree,
			fieldofstudy,
			from,
			to,
			current,
			description,
		};

		if (education) profileFields.education = educationFields;

		let profile = await ProfessionalProfile.findOne({ user: req.user._id });

		if (profile) {
			profile = await ProfessionalProfile.findOneAndUpdate(
				{ user: req.user._id },
				{ $set: profileFields },
				{ new: true }
			);

			return res.status(200).json({
				message: 'ProfessionalProfile updated',
				profile: profile,
				success: true,
			});
		}

		profile = new ProfessionalProfile(profileFields);
		await profile.save();

		res.status(201).json({
			success: true,
			data: profile,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Delete user profile
 * @route DELETE /api/profiles/delete/:user_id
 * @access Private
 */

const deleteProfile = asyncHandler(async (req, res) => {
	try {
		await Promise.all([
			Profile.findOneAndRemove({ user: req.user._id }),
			User.findOneAndRemove({ _id: req.user._id }),
			Post.deleteMany({ user: req.user._id }),
		]);

		res.status(200).json({
			message: 'User deleted',
			success: true,
			data: {},
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Add experience to profile
 * @route POST /api/profiles/experience/:user_id
 * @access Private
 */

const addExperience = asyncHandler(async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user._id });

		if (profile) {
			profile.experience.unshift(req.body);
			await profile.save();
			res.status(200).json({
				message: 'Experience added',
				success: true,
				data: profile,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Profile not found',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Delete experience from profile
 * @route DELETE /api/profiles/experience/:user_id/:exp_id
 * @access Private
 */

const deleteExperience = asyncHandler(async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user._id });

		if (profile) {
			const removeIndex = profile.experience
				.map(item => item.id)
				.indexOf(req.params.exp_id);

			profile.experience.splice(removeIndex, 1);
			res.status(200).json({
				message: 'Experience deleted',
				success: true,
				data: profile,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Add education to profile
 * @route POST /api/profiles/education/:user_id
 * @access Private
 */

const addEducation = asyncHandler(async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user._id });
		profile.education.unshift(req.body);

		await profile.save();

		res.status(200).json({
			message: 'Education added',
			success: true,
			data: profile,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Delete education from profile
 * @route DELETE /api/profiles/education/:user_id/:edu_id
 * @access Private
 */

const deleteEducation = asyncHandler(async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user._id });

		if (profile) {
			const removeIndex = profile.education
				.map(item => item.id)
				.indexOf(req.params.edu_id);

			profile.education.splice(removeIndex, 1);
			res.status(200).json({
				message: 'Education deleted',
				success: true,
				data: profile,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	getAllProfiles,
	getProfile,
	createUpdateProfile,
	deleteProfile,
	addExperience,
	deleteExperience,
	addEducation,
	deleteEducation,
};
