import asyncHandler from 'express-async-handler';
import Profession from '../models/Profession.js';

/**
 * @description Fetch all professions
 * @route GET /api/professions
 * @access Public
 * @returns {Object} Professions
 */

const getAllProfessions = asyncHandler(async (req, res) => {
	try {
		const pageSize = 10;
		const page = Number(req.query.pageNumber) || 1;

		const keyword = req.query.pageNumber
			? {
					name: {
						$regex: req.query.keyword,
						$options: 'i',
					},
			  }
			: {};

		const count = await Profession.countDocuments({ ...keyword });
		const professions = await Profession.find({ ...keyword })
			.limit(pageSize)
			.skip(pageSize * (page - 1));

		res.status(200).json({
			message: 'Professions fetched successfully',
			professions: professions,
			page: page,
			pages: Math.ceil(count / pageSize),
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching professions',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Fetch single profession
 * @route GET /api/professions/:id
 * @access Public
 * @returns {Object} Profession
 */

const getProfession = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const profession = await Profession.findById(id);
		res.status(200).json({
			message: 'Profession fetched successfully',
			profession: profession,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Profession',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Create a new profession
 * @route POST /api/professions
 * @access Private/Professional
 * @returns {Object} Profession
 */

//@des Create a profession
//@route POST /api/doctors/:id
//@access Private/Professional

const createProfession = asyncHandler(async (req, res) => {
	try {
		const { professionName, specialty, contact, numReviews } = req.body;
		const author = req.user._id;
		const profession = await Profession.create({
			professionName: professionName,
			author: author,
			specialty: specialty,
			contact: contact,
			numReviews: numReviews,
		});
		res.status(200).json({
			message: 'Profession created successfully',
			profession: profession,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error creating Profession',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Update a profession
 * @route PUT /api/professions/update/:id
 * @access Private/Professional
 * @returns {Object} Profession
 */

const updateProfession = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const { professionName, specialty, contact } = req.body;
		const profession = await Profession.findByIdAndUpdate(id, {
			professionName: professionName,
			specialty: specialty,
			contact: contact,
		});
		res.status(200).json({
			message: 'Profession updated successfully',
			profession: profession,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error updating Profession',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Delete a profession
 * @route DELETE /api/professions/delete/:id
 * @access Private/Admin
 */

const deleteProfession = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		await Profession.findByIdAndDelete(id);
		res.status(200).json({
			message: 'Profession deleted successfully',
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error deleting Profession',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Create new review
 * @route POST /api/professions/create/:id/reviews
 * @access Private
 * @returns {Object} Professional Review
 */

const createProfessionReview = asyncHandler(async (req, res) => {
	try {
		const { rating, comment } = req.body;
		const profession = await Profession.findById(req.params.id);

		if (profession) {
			const alreadyReviewed = profession.reviews.find(
				r => r.user.toString() === req.user._id.toString()
			);
			if (alreadyReviewed) {
				res.status(500).json({
					message: 'You have already reviewed this profession',
					profession: profession,
					success: false,
				});
			}

			const review = {
				username: req.user.username,
				rating: Number(rating),
				comment: comment,
				user: req.user._id,
			};

			profession.reviews.push(review);

			profession.numReviews = profession.reviews.length;

			profession.rating =
				profession -
				reviews.reduce((acc, item) => item.rating + acc, 0) /
					profession.reviews.length;

			await profession.save();
			res.status(201).json({
				message: 'Review Added',
				profession: profession.reviews.blue,
				success: true,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Error adding review',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Get top rated professionals
 * @route GET /api/professions/top
 * @access Public
 * @returns {Object} Profession
 */

const getTopProfession = asyncHandler(async (req, res) => {
	try {
		const profession = await Profession.find({}).sort({ rating: -1 }).limit(3);
		res.status(200).json({
			message: 'Top rated professions fetched successfully',
			profession: profession,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching top rated professions',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	getAllProfessions,
	getProfession,
	createProfession,
	updateProfession,
	deleteProfession,
	createProfessionReview,
	getTopProfession,
};
