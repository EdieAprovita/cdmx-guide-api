import asyncHandler from 'express-async-handler';
import User from '../models/User.js';
import generateToken from '../utils/generateToken.js';

/**
 * @description Authenticate user and get token
 * @route POST /api/auth/login
 * @access Public
 * @returns {Object} User
 */

const loginUser = asyncHandler(async (req, res) => {
	try {
		const { email, password } = req.body;

		const user = await User.findOne({ email });

		if (user && (await user.matchPassword(password))) {
			res.status(200).json({
				_id: user._id,
				username: user.username,
				email: user.email,
				role: user.role,
				isAdmin: user.isAdmin,
				isProfessional: user.isProfessional,
				token: generateToken(user._id),
			});
		}
	} catch (error) {
		res.status(401).json({
			message: 'Invalid credentials',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Register a new user
 * @route POST /api/auth/register
 * @access Public
 * @returns {Object} User
 */

const registerUser = asyncHandler(async (req, res) => {
	try {
		const { username, email, password, role } = req.body;

		const userExists = await User.findOne({ email });

		if (userExists) {
			res.status(500).json({
				message: 'User already exists',
				success: false,
			});
		}

		const user = await User.create({
			username: username,
			email: email,
			password: password,
			role: role,
		});

		if (user) {
			res.status(201).json({
				_id: user._id,
				name: user.username,
				email: user.email,
				role: user.role,
				isAdmin: user.isAdmin,
				isProfessional: user.isProfessional,
				token: generateToken(user._id),
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'The user could not be created',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Get user profile
 * @route GET /api/auth/me
 * @access Private
 * @returns {Object} User
 */

const getUserProfile = asyncHandler(async (req, res) => {
	const user = await User.findById(req.user._id);

	if (user) {
		res.status(200).json({
			message: `User profile ${user.username}`,
			user: user,
			success: true,
		});
	} else {
		res.status(404).json({
			message: 'User not found',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Update user profile
 * @route PUT /api/auth/update/me
 * @access Private
 * @returns {Object} User
 */

const updateUserProfile = asyncHandler(async (req, res) => {
	try {
		const user = await User.findById(req.user._id);

		if (user) {
			user.username = req.body.username || user.username;
			user.email = req.body.email || user.email;

			if (req.body.password) {
				user.password = req.body.password;
			}

			const updatedUser = await user.save();

			res.status(200).json({
				_id: updatedUser._id,
				username: updatedUser.username,
				email: updatedUser.email,
				isAdmin: updatedUser.isAdmin,
				isProfessional: updatedUser.isProfessional,
				token: generateToken(updatedUser._id),
			});
		}
	} catch (error) {
		res.status(404).json({
			message: 'User not updated',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Get all users
 * @route GET /api/auth/users
 * @access Private/Admin
 * @returns {Object} Users
 */

const getUsers = asyncHandler(async (req, res) => {
	try {
		const users = await User.find({});
		res.status(200).json({
			message: 'All users',
			users: users,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Users not found',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Delete user
 * @route DELETE /api/auth/delete/:id
 * @access Private/Admin
 */

const deleteUser = asyncHandler(async (req, res) => {
	try {
		const user = await User.findById(req.params.id);

		if (user) {
			await user.remove();
			res.status(200).json({ message: 'User removed', success: true });
		}
	} catch (error) {
		res.status(404).json({
			message: 'User not deleted',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Get user by id
 * @route GET /api/auth/user/:id
 * @access Private/Admin
 * @returns {Object} User
 */

const getUserById = asyncHandler(async (req, res) => {
	try {
		const user = await User.findById(req.params.id).select('-password');

		if (user) {
			res.status(200).json({
				message: `User found by ID ${user.username}`,
				user: user,
				success: true,
			});
		}
	} catch (error) {
		res.status(404).json({
			message: 'User not found by Id',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Update user by id
 * @route PUT /api/auth/update/:id
 * @access Private/Admin
 * @returns {Object} User
 */

const updateUser = asyncHandler(async (req, res) => {
	try {
		const user = await User.findById(req.params.id);

		if (user) {
			user.username = req.body.username || user.username;
			user.email = req.body.email || user.email;
			user.isAdmin = req.body.isAdmin;
			user.isProfessional = req.body.isProfessional;

			const updatedUser = await user.save();

			res.status(200).json({
				_id: updatedUser._id,
				username: updatedUser.username,
				email: updatedUser.email,
				isAdmin: updatedUser.isAdmin,
			});
		}
	} catch (error) {
		res.status(404).json({
			message: 'User not updated',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	loginUser,
	registerUser,
	getUserProfile,
	updateUserProfile,
	getUsers,
	deleteUser,
	getUserById,
	updateUser,
};
