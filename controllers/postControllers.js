import asyncHandler from 'express-async-handler';

import Post from '../models/Post.js';
import User from '../models/User.js';

/**
 * @description Fetch all posts
 * @route GET /api/posts
 * @access Private
 * @returns {Object} Posts
 */

const getAllPosts = asyncHandler(async (req, res, next) => {
	try {
		const posts = await Post.find().populate({
			path: 'user',
			select: 'name',
		});
		res.status(200).json({
			success: true,
			count: posts.length,
			data: posts,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: error.message,
		});
	}
});

/**
 * @description Fetch single post
 * @route GET /api/posts/create/:id
 * @access Private
 * @returns {Object} Post
 */

const createPost = asyncHandler(async (req, res) => {
	try {
		const user = await User.findById(req.user._id).select('-password');

		const newPost = await Post.create({
			user: req.user._id,
			text: req.body.text,
			name: user.name,
			avatar: user.avatar,
		});

		const post = await newPost.save();

		res.status(201).json({
			message: 'Post created',
			post: post,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: error.message,
		});
	}
});

/**
 * @description Fetch single post by Id
 * @route GET /api/posts/:id
 * @access Private
 * @returns {Object} Post
 */

const getPostById = asyncHandler(async (req, res) => {
	try {
		const post = await Post.findById(req.params.id).populate('user', [
			'name',
			'avatar',
		]);

		if (!post) {
			return res.status(400).json({
				message: 'No post found',
				success: false,
			});
		}

		res.status(200).json({
			message: 'Post fetched',
			post: post,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			success: false,
			error: 'Server Error',
			message: error.message,
		});
	}
});

/**
 * @description Delete post by Id
 * @route DELETE /api/posts/:id
 * @access Private/Admin
 */

const deletePost = asyncHandler(async (req, res) => {
	try {
		const post = await Post.findById(req.params.id);

		if (!post) {
			return res.status(400).json({
				message: 'No post found',
				success: false,
			});
		}

		// Check user
		if (post.user.toString() !== req.user._id.toString()) {
			return res.status(401).json({
				message: 'User not authorized',
				user: post.user,
				success: false,
			});
		}

		await post.remove();

		res.status(200).json({
			message: 'Post removed',
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: error.message,
		});
	}
});

/**
 * @description Create a comment on a post
 * @route POST /api/posts/comment/:id
 * @access Private
 * @returns {Object} Comment
 */

const commentOnPost = asyncHandler(async (req, res) => {
	try {
		const user = await User.findById(req.user._id).select('-password');
		const post = await Post.findById(req.params.id);

		const newComment = {
			text: req.body.text,
			name: user.name,
			avatar: user.avatar,
			user: req.user._id,
		};

		post.comments.unshift(newComment);

		await post.save();

		res.status(200).json({
			message: 'Comment added',
			newComment: newComment,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: error.message,
		});
	}
});

/**
 * @description Delete comment on a post
 * @route DELETE /api/posts/comment/:id/:comment_id
 * @access Private/Admin
 */

const deleteComment = asyncHandler(async (req, res) => {
	try {
		const post = await Post.findById(req.params.id);

		// Pull out comment
		const comment = post.comments.find(
			comment => comment.id === req.params.comment_id
		);

		// Make sure comment exists
		if (!comment) {
			return res.status(404).json({
				message: 'No comment found',
				success: false,
			});
		}

		// Check user
		if (comment.user.toString() !== req.user._id.toString()) {
			return res.status(401).json({
				message: 'User not authorized',
				user: comment.user,
				success: false,
			});
		}

		// Get remove index
		const removeIndex = post.comments
			.map(comment => comment.user.toString())
			.indexOf(req.user._id.toString());

		post.comments.splice(removeIndex, 1);

		await post.save();

		res.status(200).json({
			message: 'Comment removed',
			post: post,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Server Error',
			success: false,
			error: error.message,
		});
	}
});

export { createPost, getAllPosts, getPostById, deletePost, commentOnPost, deleteComment };
