import asyncHandler from 'express-async-handler';
import Doctor from '../models/Doctor.js';

/**
 * @description    Fetch all doctors
 * @route   GET /api/doctors
 * @access  Public
 * @returns {Object} Doctors
 */

const getAllDoctors = asyncHandler(async (req, res) => {
	try {
		const pageSize = 10;
		const page = Number(req.query.pageNumber) || 1;

		const keyword = req.query.pageNumber
			? {
					name: {
						$regex: req.query.keyword,
						$options: 'i',
					},
			  }
			: {};

		const count = await Doctor.countDocuments({ ...keyword });
		const doctors = await Doctor.find({ ...keyword })
			.limit(pageSize)
			.skip(pageSize * (page - 1));

		res.status(200).json({
			message: 'Doctors fetched successfully',
			doctors: doctors,
			page: page,
			pages: Math.ceil(count / pageSize),
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Doctors',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Fetch single doctor
 * @route   GET /api/doctors/:id
 * @access  Public
 * @returns {Object} Doctor
 */

const getDoctor = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const doctor = await Doctor.findById(id);
		res.status(200).json({
			message: 'Doctor fetched successfully',
			doctor: doctor,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Doctor',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Create doctor
 * @route   POST /api/doctors/create
 * @access  Private
 * @returns {Object} Doctor
 */

const createDoctor = asyncHandler(async (req, res) => {
	try {
		const { name, address, image, specialty, contact, numReviews } = req.body;
		const author = req.user._id;
		const doctor = await Doctor.create({
			name,
			author,
			address,
			image,
			specialty,
			contact,
			numReviews,
		});
		res.status(201).json({
			message: 'Doctor created successfully',
			doctor: doctor,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error creating doctor',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Update a doctor
 * @route   PUT /api/doctors/update/:id
 * @access  Private/Admin
 * @returns {Object} Doctor
 */

const updateDoctor = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const { name, address, image, specialty, contact } = req.body;
		const doctor = await Doctor.findByIdAndUpdate(id, {
			name,
			address,
			image,
			specialty,
			contact,
		});
		res.status(200).json({
			message: 'Doctor updated successfully',
			doctor: doctor,
			success: true,
		});
	} catch (error) {
		res.status(500).json({ message: `${error}`.red });
	}
});

/**
 * @description    Delete a doctor
 * @route   DELETE /api/doctors/delete/:id
 * @access  Private/Admin
 */

const deleteDoctor = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		await Doctor.findByIdAndDelete(id);
		res.status(200).json({ message: 'Delete doctor', success: true });
	} catch (error) {
		res.status(500).json({ message: `${error}`.red });
	}
});

/**
 * @description    Create a review for a doctor
 * @route   POST /api/doctors/created/:id/reviews
 * @access  Private
 * @returns {Object} Doctor Review
 */

const createDoctorReview = asyncHandler(async (req, res) => {
	try {
		const { rating, comment } = req.body;
		const doctor = await Doctor.findById(req.params.id);

		if (doctor) {
			const alreadyReviewed = doctor.reviews.find(
				r => r.user.toString() === req.user._id.toString()
			);
			if (alreadyReviewed) {
				res.status(500).json({
					message: 'You have already reviewed this Doctor',
					doctor: doctor,
					success: false,
				});
			}

			const review = {
				username: req.user.username,
				rating: Number(rating),
				comment,
				user: req.user._id,
			};

			doctor.reviews.push(review);

			doctor.numReviews = doctor.reviews.length;

			doctor.rating =
				doctor -
				reviews.reduce((acc, item) => item.rating + acc, 0) /
					doctor.reviews.length;

			await doctor.save();
			res.status(201).json({
				message: 'Review Added',
				doctor: doctor,
				success: true,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Error adding Review',
			success: false,
			error: `${error}`.red,
		});
	}
});

// @desc    Get top rated doctors
// @route   GET /api/doctors/top
// @access  Public

const getTopDoctor = asyncHandler(async (req, res) => {
	try {
		const doctor = await Doctor.find({}).sort({ rating: -1 }).limit(3);
		res.status(200).json({
			message: 'Top rated Doctors fetched successfully',
			doctor: doctor,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Doctors',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	getAllDoctors,
	getDoctor,
	createDoctor,
	updateDoctor,
	deleteDoctor,
	createDoctorReview,
	getTopDoctor,
};
