import asyncHandler from 'express-async-handler';
import Recipe from '../models/Recipe.js';

/**
 * @description Fetch all recipes
 * @route GET /api/recipes
 * @access Public
 * @returns {Object} Recipes
 */

const getAllRecipes = asyncHandler(async (req, res) => {
	try {
		const pageSize = 10;
		const page = Number(req.query.pageNumber) || 1;

		const keyword = req.query.pageNumber
			? {
					name: {
						$regex: req.query.keyword,
						$options: 'i',
					},
			  }
			: {};

		const count = await Recipe.countDocuments({ ...keyword });
		const recipes = await Recipe.find({ ...keyword })
			.limit(pageSize)
			.skip(pageSize * (page - 1));

		res.status(200).json({
			message: 'Recipes fetched successfully',
			data: recipes,
			page: page,
			pages: Math.ceil(count / pageSize),
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Recipes',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Fetch single recipe
 * @route GET /api/recipes/:id
 * @access Public
 * @returns {Object} Recipe
 */

const getRecipe = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const recipe = await Recipe.findById(id);
		res.status(200).json({
			message: 'Recipe fetched successfully',
			recipe: recipe,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Recipe',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Create new recipe
 * @route POST /api/recipes/create
 * @access Private
 * @returns {Object} Recipe
 */

const createRecipe = asyncHandler(async (req, res) => {
	try {
		const {
			title,
			description,
			instructions,
			ingredients,
			typeDish,
			image,
			cookingTime,
			difficulty,
			budget,
			numReviews,
		} = req.body;
		const author = req.user._id;

		const recipe = await Recipe.create({
			title: title,
			author: author,
			description: description,
			instructions: instructions,
			ingredients: ingredients,
			typeDish: typeDish,
			image: image,
			cookingTime: cookingTime,
			difficulty: difficulty,
			budget: budget,
			numReviews: numReviews,
		});

		res.status(201).json({
			message: 'Recipe created successfully',
			recipe: recipe,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error creating Recipe',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Update recipe
 * @route PUT /api/recipes/update/:id
 * @access Private
 * @returns {Object} Recipe
 */

const updateRecipe = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const {
			title,
			description,
			instructions,
			ingredients,
			typeDish,
			image,
			cookingTime,
			difficulty,
		} = req.body;

		const recipe = await Recipe.findByIdAndUpdate(id, {
			title: title,
			description: description,
			instructions: instructions,
			ingredients: ingredients,
			typeDish: typeDish,
			image: image,
			cookingTime: cookingTime,
			difficulty: difficulty,
		});

		res.status(200).json({
			message: 'Recipe updated successfully',
			recipe: recipe,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error updating Recipe',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Delete recipe
 * @route DELETE /api/recipes/delete/:id
 * @access Private
 */

const deleteRecipe = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		await Recipe.findByIdAndDelete(id);
		res.status(200).json({
			message: 'Recipe deleted successfully',
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error deleting Recipe',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Create a new review
 * @route POST /api/recipes/review/:id
 * @access Private
 * @returns {Object} Recipe
 */

const createRecipeReview = asyncHandler(async (req, res) => {
	try {
		const { rating, comment } = req.body;

		const recipe = await Recipe.findById(req.params.id);

		if (recipe) {
			const alreadyReviewed = recipe.reviews.find(
				r => r.user.toString() === req.user._id.toString()
			);

			if (alreadyReviewed) {
				res.status(500).json({
					message: 'You have already reviewed this Recipe',
					recipe: recipe,
					success: false,
				});
			}

			const review = {
				username: req.user.username,
				rating: Number(rating),
				comment: comment,
				user: req.user._id,
			};

			recipe.reviews.push(review);
			recipe.numReviews = recipe.reviews.length;
			recipe.rating =
				recipe.reviews.reduce((acc, item) => item.rating + acc, 0) /
				recipe.reviews.length;

			await recipe.save();
			res.status(201).json({
				message: 'Review created successfully',
				recipe: recipe.reviews.blue,
				success: true,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Error creating review',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description Get top recipes
 * @route GET /api/recipes/top
 * @access Public
 * @returns {Object} Recipe
 */

const getTopRecipes = asyncHandler(async (req, res) => {
	try {
		const recipes = await Recipe.find({}).sort({ rating: -1 }).limit(3);
		res.status(200).json({
			message: 'Top rated recipes fetched successfully',
			recipes: recipes,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching top rated recipes',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	getAllRecipes,
	getRecipe,
	createRecipe,
	updateRecipe,
	deleteRecipe,
	createRecipeReview,
	getTopRecipes,
};
