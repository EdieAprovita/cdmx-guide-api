import asyncHandler from 'express-async-handler';
import Business from '../models/Business.js';

/**
 *@description    Fetch all businesses
 *@route   GET /api/businesses
 *@access  Public
 *@returns {Object} Businesses
 */

const getAllBusiness = asyncHandler(async (req, res) => {
	try {
		const pageSize = 10;
		const page = Number(req.query.pageNumber) || 1;

		const keyword = req.query.keyword
			? {
					name: {
						$regex: req.query.keyword,
						$options: 'i',
					},
			  }
			: {};

		const count = await Business.countDocuments({ ...keyword });
		const business = await Business.find({ ...keyword })
			.limit(pageSize)
			.skip(pageSize * (page - 1));

		res.status(200).json({
			message: 'Businesses fetched successfully',
			business: business,
			page: page,
			pages: Math.ceil(count / pageSize),
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Businesses',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 *@description    Fetch single business
 *@route   GET /api/businesses/:id
 *@access  Public
 *@returns {Object} Business
 */

const getBusiness = asyncHandler(async (req, res) => {
	try {
		const business = await Business.findById(req.params.id);
		res.status(200).json({
			message: 'Business fetched successfully',
			business: business,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Business',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Create new business
 * @route   POST /api/businesses/create
 * @access  Private
 * @returns {Object} Business
 */

const createBusiness = asyncHandler(async (req, res) => {
	try {
		const { name, address, contact, image, budget, typeBusiness, numReviews } =
			req.body;
		const author = req.user._id;
		const business = await Business.create({
			name: name,
			author: author,
			address: address,
			contact: contact,
			image: image,
			budget: budget,
			typeBusiness: typeBusiness,
			numReviews: numReviews,
		});
		res.status(201).json({
			message: 'Business created successfully',
			business: business,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Business creation failed',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Update business
 * @route   PUT /api/businesses/update/:id
 * @access  Private/Admin
 * @return {Object} Business
 */

const updateBusiness = asyncHandler(async (req, res) => {
	try {
		const { name, address, contact, image, budget, typeBusiness } = req.body;
		const business = await Business.findByIdAndUpdate(req.params.id, {
			name: name,
			address: address,
			contact: contact,
			image: image,
			budget: budget,
			typeBusiness: typeBusiness,
		});
		res.status(200).json({
			message: 'Business updated successfully',
			business: business,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Business was not updated',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Delete business
 * @route   DELETE /api/businesses/delete/:id
 * @access  Private/Admin
 */

const deleteBusiness = asyncHandler(async (req, res) => {
	try {
		await Business.findByIdAndDelete(req.params.id);
		res.status(200).json({ message: 'Deleted Business', success: true });
	} catch (error) {
		res.status(500).json({
			message: 'Error deleting Business',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Create new business review
 * @route   POST /api/businesses/create/:id/reviews
 * @access  Private
 * @returns {Object} Business Review
 */

const createBusinessReview = asyncHandler(async (req, res) => {
	try {
		const { rating, comment } = req.body;

		const business = await Business.findById(req.params.id);

		if (business) {
			const alreadyReviewed = business.reviews.find(
				r => r.user.toString() === req.user._id.toString()
			);
			if (alreadyReviewed) {
				res.status(500).json({
					message: 'You have already reviewed this Business',
					business: business,
					success: false,
				});
			}

			const review = {
				username: req.user.username,
				rating: Number(rating),
				comment: comment,
				user: req.user._id,
			};

			business.reviews.push(review);

			business.numReviews = business.reviews.length;

			business.rating =
				business -
				reviews.reduce((acc, item) => item.rating + acc, 0) /
					business.reviews.length;

			await business.save();
			res.status(201).json({
				message: 'Review Added',
				business: business,
				success: true,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Error adding Review',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Get top rated businesses
 * @route   GET /api/businesses/top
 * @access  Public
 * @returns {Object} Business
 */

const getTopBusiness = asyncHandler(async (req, res) => {
	try {
		const business = await Business.find({}).sort({ rating: -1 }).limit(3);
		res.status(200).json({
			message: 'Top rated Businesses fetched successfully',
			business: business,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Businesses',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	getAllBusiness,
	getBusiness,
	createBusiness,
	updateBusiness,
	deleteBusiness,
	createBusinessReview,
	getTopBusiness,
};
