import asyncHandler from 'express-async-handler';
import Market from '../models/Market.js';

/**
 * @description    Fetch all markets
 * @route   GET /api/markets
 * @access  Public
 * @returns {Object} Markets
 */

const getAllMarkets = asyncHandler(async (req, res) => {
	try {
		const pageSize = 10;
		const page = Number(req.query.pageNumber) || 1;

		const keyword = req.query.pageNumber
			? {
					name: {
						$regex: req.query.keyword,
						$options: 'i',
					},
			  }
			: {};

		const count = await Market.countDocuments({ ...keyword });
		const markets = await Market.find({ ...keyword })
			.limit(pageSize)
			.skip(pageSize * (page - 1));
		res.status(200).json({
			message: 'Markets fetched successfully',
			markets: markets,
			page: page,
			pages: Math.ceil(count / pageSize),
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching markets',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Fetch single market
 * @route   GET /api/markets/:id
 * @access  Public
 * @returns {Object} Market
 */

const getMarket = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const market = await Market.findById(id).populate('User');
		res.status(200).json({
			message: 'Market fetched successfully',
			market: market,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching Market',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Create a new market
 * @route   POST /api/markets/create
 * @access  Private
 * @returns {Object} Market
 */

const createMarket = asyncHandler(async (req, res) => {
	try {
		const { name, address, image, typeMarket, numReviews } = req.body;
		const author = req.user._id;

		const market = await Market.create({
			name: name,
			author: author,
			address: address,
			image: image,
			typeMarket: typeMarket,
			numReviews: numReviews,
		});
		res.status(201).json({
			message: 'Market created successfully',
			market: market,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error creating Market',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Update a market
 * @route   PUT /api/markets/:id
 * @access  Private/Admin
 * @returns {Object} Market
 */

const updateMarket = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		const { name, address, image, typeMarket } = req.body;
		const market = await Market.findByIdAndUpdate(id, {
			name: name,
			address: address,
			image: image,
			typeMarket: typeMarket,
		});
		res.status(200).json({
			message: 'Market updated successfully',
			market: market,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error updating Market',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Delete a market
 * @route   DELETE /api/markets/delete/:id
 * @access  Private/Admin
 */

const deleteMarket = asyncHandler(async (req, res) => {
	try {
		const { id } = req.params;
		await Market.findOneAndDelete(id);
		res.status(200).json({
			message: 'Market deleted successfully',
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error deleting Market',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Create a new review
 * @route   POST /api/markets/create/:id/reviews
 * @access  Private
 * @returns {Object} Market Review
 */

const createMarketReview = asyncHandler(async (req, res) => {
	try {
		const { rating, comment } = req.body;

		const market = await Market.findById(req.params.id);

		if (market) {
			const alreadyReviewed = market.reviews.find(
				r => r.user.toString() === req.user._id.toString()
			);

			if (alreadyReviewed) {
				res.status(500).json({
					message: 'You have already reviewed this market',
					market: market,
					success: false,
				});
			}

			const review = {
				username: req.user.username,
				rating: Number(rating),
				comment: comment,
				user: req.user._id,
			};

			market.reviews.push(review);
			market.numReviews = market.reviews.length;
			market.rating =
				market.reviews.reduce((acc, item) => item.rating + acc, 0) /
				market.reviews.length;

			await market.save();
			res.status(201).json({
				message: 'Review created successfully',
				market: market,
				success: true,
			});
		}
	} catch (error) {
		res.status(500).json({
			message: 'Error adding Review',
			success: false,
			error: `${error}`.red,
		});
	}
});

/**
 * @description    Get top rated markets
 * @route   GET /api/markets/top
 * @access  Public
 * @returns {Object} Markets
 */

const getTopMarkets = asyncHandler(async (req, res) => {
	try {
		const markets = await Market.find({}).sort({ rating: -1 }).limit(3);
		res.status(200).json({
			message: 'Top markets fetched successfully',
			markets: markets,
			success: true,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Error fetching top markets',
			success: false,
			error: `${error}`.red,
		});
	}
});

export {
	getAllMarkets,
	getMarket,
	createMarket,
	updateMarket,
	deleteMarket,
	createMarketReview,
	getTopMarkets,
};
