import colors from 'colors';
import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import path from 'path';
colors.enabled = true;

import connectDB from './config/db.js';
import { errorHandler, notFound } from './middleware/errorMiddleware.js';
import {
	authRouter,
	businessRoutes,
	doctorRoutes,
	marketRoutes,
	postRoutes,
	professionalProfileRoutes,
	professionRoutes,
	recipeRoutes,
	restaurantsRoutes,
	uploadRoutes,
} from './routes/index.js';

dotenv.config();

connectDB();

const app = express();

if (process.env.NODE_ENV === 'development') {
	app.use(morgan('dev'));
}

app.use(express.json());

//CORS
app.use(
	cors({
		credentials: true,
		origin: ['http://localhost:3000'],
	})
);

const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

if (process.env.NODE_ENV === 'production') {
	app.use(express.static(path.join(__dirname, 'client/build')));

	app.get('*', (req, res) =>
		res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html'))
	);
} else {
	app.get('/', (req, res) => {
		res.send('API is running');
	});
}
/**
 * MIDDLEWARE FOR NOT FOUND ROUTES & HANDLE ERRORS
 */
app.use(notFound);
app.use(errorHandler);

//ROUTES FOR API
app.use('/api/recipes', recipeRoutes);
app.use('/api/restaurants', restaurantsRoutes);
app.use('/api/auth', authRouter);
app.use('/api/markets', marketRoutes);
app.use('/api/businesses', businessRoutes);
app.use('/api/doctors', doctorRoutes);
app.use('/api/professions', professionRoutes);
app.use('/api/posts', postRoutes);
app.use('/api/profiles', professionalProfileRoutes);
app.use('/api/upload', uploadRoutes);

//app.get('/*', (req, res) => res.sendFile(__dirname + '/public/index.html'))

const PORT = process.env.PORT || 5000;

app.listen(
	PORT,
	console.log(
		`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
	)
);
