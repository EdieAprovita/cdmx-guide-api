import businessSchema from '../../models/Business.js';
import doctorSchema from '../../models/Doctor.js';
import marketSchema from '../../models/Market.js';
import professionSchema from '../../models/Profession.js';
import recipeSchema from '../../models/Recipe.js';
import restaurantSchema from '../../models/Restaurant.js';
import reviewSchema from '../../models/Review.js';
import userSchema from '../../models/User.js';

describe('Check Models', () => {
	test('Check Business schema should be defined', () => {
		expect(businessSchema).toBeDefined();
	});

	test('Check Doctor Schema should be defined', () => {
		expect(doctorSchema).toBeDefined();
	});

	test('Check Profession Schema should be defined', () => {
		expect(professionSchema).toBeDefined();
	});

	test('Check Recipe Schema should be defined', () => {
		expect(recipeSchema).toBeDefined();
	});

	test('Check Restaurant Schema should be defined', () => {
		expect(restaurantSchema).toBeDefined();
	});

	test('Check ReviewSchema should be defined', () => {
		expect(reviewSchema).toBeDefined();
	});

	test('Check Market Schema should be defined', () => {
		expect(marketSchema).toBeDefined();
	});

	test('Check UserSchema', () => {
		expect(userSchema).toBeDefined();
	});
});
