import {
	getAllDoctors,
	getDoctor,
	createDoctor,
	updateDoctor,
	deleteDoctor,
	createDoctorReview,
	getTopDoctor,
} from '../../controllers/doctorsControllers.js';
import { jest } from '@jest/globals';

jest.useFakeTimers();

describe('Doctors Controllers', () => {
	test('getAllDoctors()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getAllDoctors(req, res, next);
		expect(getAllDoctors).toBeDefined();
	});

	test('getDoctor()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getDoctor(req, res, next);
		expect(getDoctor).toBeDefined();
	});

	test('createDoctor()', () => {
		const req = {
			body: {
				name: 'test',
				address: 'test',
				city: 'test',
				state: 'test',
				zipcode: 'test',
				category: 'test',
				image: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createDoctor(req, res, next);
		expect(createDoctor).toBeDefined();
	});

	test('updateDoctor()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				name: 'test',
				address: 'test',
				city: 'test',
				state: 'test',
				zipcode: 'test',
				category: 'test',
				image: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		updateDoctor(req, res, next);
		expect(updateDoctor).toBeDefined();
	});

	test('deleteDoctor()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteDoctor(req, res, next);
		expect(deleteDoctor).toBeDefined();
	});

	test('createDoctorReview()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				rating: 1,
				review: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createDoctorReview(req, res, next);
		expect(createDoctorReview).toBeDefined();
	});

	test('getTopDoctor()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getTopDoctor(req, res, next);
		expect(getTopDoctor).toBeDefined();
	});
});
