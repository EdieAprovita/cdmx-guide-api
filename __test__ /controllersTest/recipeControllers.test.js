import {
	getAllRecipes,
	getRecipe,
	createRecipe,
	updateRecipe,
	deleteRecipe,
	createRecipeReview,
	getTopRecipes,
} from '../../controllers/recipesControllers.js';
import { jest } from '@jest/globals';

jest.useFakeTimers();

describe('Recipes Controller', () => {
	test('getAllRecipes()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getAllRecipes(req, res, next);
		expect(getAllRecipes).toBeDefined();
	});

	test('getRecipe()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getRecipe(req, res, next);
		expect(getRecipe).toBeDefined();
	});

	test('createRecipe()', () => {
		const req = {
			body: {
				name: 'test',
				description: 'test',
				image: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createRecipe(req, res, next);
		expect(createRecipe).toBeDefined();
	});

	test('updateRecipe()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				name: 'test',
				description: 'test',
				image: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		updateRecipe(req, res, next);
		expect(updateRecipe).toBeDefined();
	});

	test('deleteRecipe()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteRecipe(req, res, next);
		expect(deleteRecipe).toBeDefined();
	});

	test('createRecipeReview()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				rating: 1,
				comment: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createRecipeReview(req, res, next);
		expect(createRecipeReview).toBeDefined();
	});

	test('getTopRecipes()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getTopRecipes(req, res, next);
		expect(getTopRecipes).toBeDefined();
	});
});
