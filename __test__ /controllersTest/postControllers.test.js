import {
	createPost,
	getPostById,
	deletePost,
	commentOnPost,
	deleteComment,
} from '../../controllers/postControllers.js';

import { jest } from '@jest/globals';

jest.useFakeTimers();
describe('Post Controllers', () => {
	test('createPost()', () => {
		const req = {
			body: {
				title: 'test',
				content: 'test',
				user: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createPost(req, res, next);
		expect(createPost).toBeDefined();
	});

	test('getPostById()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getPostById(req, res, next);
		expect(getPostById).toBeDefined();
	});

	test('deletePost()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deletePost(req, res, next);
		expect(deletePost).toBeDefined();
	});

	test('commentOnPost()', () => {
		const req = {
			body: {
				content: 'test',
				user: 'test',
				post: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		commentOnPost(req, res, next);
		expect(commentOnPost).toBeDefined();
	});

	test('deleteComment()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteComment(req, res, next);
		expect(deleteComment).toBeDefined();
	});
});
