import {
	getAllBusiness,
	getBusiness,
	createBusiness,
	updateBusiness,
	deleteBusiness,
	createBusinessReview,
	getTopBusiness,
} from '../../controllers/businessControllers.js';
import { jest } from '@jest/globals';

jest.useFakeTimers();
describe('Business Controllers', () => {
	test('getAllBusiness()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getAllBusiness(req, res, next);
		expect(getAllBusiness).toBeDefined();
	});

	test('getBusiness()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getBusiness(req, res, next);
		expect(getBusiness).toBeDefined();
	});

	test('createBusiness()', () => {
		const req = {
			body: {
				name: 'test',
				address: 'test',
				city: 'test',
				state: 'test',
				zipcode: 'test',
				category: 'test',
				image: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createBusiness(req, res, next);
		expect(createBusiness).toBeDefined();
	});

	test('updateBusiness()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				name: 'test',
				address: 'test',
				city: 'test',
				state: 'test',
				zipcode: 'test',
				category: 'test',
				image: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		updateBusiness(req, res, next);
		expect(updateBusiness).toBeDefined();
	});

	test('deleteBusiness()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteBusiness(req, res, next);
		expect(deleteBusiness).toBeDefined();
	});

	test('createBusinessReview()', () => {
		const req = {
			body: {
				businessId: 1,
				userId: 1,
				rating: 1,
				review: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createBusinessReview(req, res, next);
		expect(createBusinessReview).toBeDefined();
	});

	test('getTopBusiness()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getTopBusiness(req, res, next);
		expect(getTopBusiness).toBeDefined();
	});
});
