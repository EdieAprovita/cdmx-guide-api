import {
	addEducation,
	addExperience,
	createUpdateProfile,
	deleteEducation,
	deleteExperience,
	deleteProfile,
	getAllProfiles,
	getProfile,
} from '../../controllers/professionalProfileControllers.js';

import { jest } from '@jest/globals';

jest.useFakeTimers();
describe('Profile Controllers', () => {
	test('getAllProfiles()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getAllProfiles(req, res, next);
		expect(getAllProfiles).toBeDefined();
	});

	test('getProfile()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getProfile(req, res, next);
		expect(getProfile).toBeDefined();
	});

	test('createUpdateProfile()', () => {
		const req = {
			body: {
				contact: {
					phone: 'test',
					email: 'test',
				},
				skills: ['test'],
				experience: {
					title: 'test',
					company: 'test',
					location: 'test',
					from: 'test',
					to: 'test',
					current: 'test',
					description: 'test',
				},
				education: {
					school: 'test',
					degree: 'test',
					fieldOfStudy: 'test',
					from: 'test',
					to: 'test',
					current: 'test',
					description: 'test',
				},
				social: {
					youtube: 'test',
					twitter: 'test',
					facebook: 'test',
					linkedin: 'test',
					instagram: 'test',
				},
				date: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createUpdateProfile(req, res, next);
		expect(createUpdateProfile).toBeDefined();
	});

	test('deleteProfile()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteProfile(req, res, next);
		expect(deleteProfile).toBeDefined();
	});

	test('addExperience()', () => {
		const req = {
			body: {
				title: 'test',
				company: 'test',
				location: 'test',
				from: 'test',
				to: 'test',
				current: 'test',
				description: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		addExperience(req, res, next);
		expect(addExperience).toBeDefined();
	});

	test('addEducation()', () => {
		const req = {
			body: {
				school: 'test',
				degree: 'test',
				fieldOfStudy: 'test',
				from: 'test',
				to: 'test',
				current: 'test',
				description: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		addEducation(req, res, next);
		expect(addEducation).toBeDefined();
	});

	test('deleteEducation()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteEducation(req, res, next);
		expect(deleteEducation).toBeDefined();
	});

	test('deleteExperience()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteExperience(req, res, next);
		expect(deleteExperience).toBeDefined();
	});
});
