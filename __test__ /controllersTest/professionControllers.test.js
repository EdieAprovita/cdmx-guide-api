import { jest } from '@jest/globals';
import {
	createProfession,
	createProfessionReview,
	deleteProfession,
	getAllProfessions,
	getProfession,
	getTopProfession,
	updateProfession,
} from '../../controllers/professionControllers.js';

jest.useFakeTimers();

describe('Profession Controllers', () => {
	test('getAllProfessions()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getAllProfessions(req, res, next);
		expect(getAllProfessions).toBeDefined();
	});

	test('getProfession()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getProfession(req, res, next);
		expect(getProfession).toBeDefined();
	});

	test('createProfession()', () => {
		const req = {
			body: {
				professionName: 'test',
				specialty: 'test',
				contact: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createProfession(req, res, next);
		expect(createProfession).toBeDefined();
	});

	test('updateProfession()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				professionName: 'test',
				specialty: 'test',
				contact: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		updateProfession(req, res, next);
		expect(updateProfession).toBeDefined();
	});

	test('deleteProfession()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteProfession(req, res, next);
		expect(deleteProfession).toBeDefined();
	});

	test('createProfessionReview()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				rating: 1,
				review: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createProfessionReview(req, res, next);
		expect(createProfessionReview).toBeDefined();
	});
	test('getTopProfession()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getTopProfession(req, res, next);
		expect(getTopProfession).toBeDefined();
	});
});
