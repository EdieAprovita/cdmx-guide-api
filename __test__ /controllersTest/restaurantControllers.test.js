import {
	getAllRestaurants,
	getRestaurant,
	createRestaurant,
	updateRestaurant,
	deleteRestaurant,
	createRestaurantReview,
	getTopRestaurants,
} from '../../controllers/restaurantsControllers.js';
import { jest } from '@jest/globals';

jest.useFakeTimers();

describe('restaurantControllers', () => {
	test('getAllRestaurants()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getAllRestaurants(req, res, next);
		expect(getAllRestaurants).toBeDefined();
	});

	test('getRestaurant()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getRestaurant(req, res, next);
		expect(getRestaurant).toBeDefined();
	});

	test('createRestaurant()', () => {
		const req = {
			body: {
				name: 'test',
				description: 'test',
				image: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createRestaurant(req, res, next);
		expect(createRestaurant).toBeDefined();
	});

	test('updateRestaurant()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		updateRestaurant(req, res, next);
		expect(updateRestaurant).toBeDefined();
	});

	test('deleteRestaurant()', () => {
		const req = {
			params: {
				id: 1,
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		deleteRestaurant(req, res, next);
		expect(deleteRestaurant).toBeDefined();
	});

	test('createRestaurantReview()', () => {
		const req = {
			params: {
				id: 1,
			},
			body: {
				rating: 1,
				comment: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		createRestaurantReview(req, res, next);
		expect(createRestaurantReview).toBeDefined();
	});

	test('getTopRestaurants()', () => {
		const req = {};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		getTopRestaurants(req, res, next);
		expect(getTopRestaurants).toBeDefined();
	});
});
