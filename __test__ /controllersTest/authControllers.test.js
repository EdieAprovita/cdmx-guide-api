import { jest } from '@jest/globals';
import {
	deleteUser,
	getUserById,
	getUserProfile,
	getUsers,
	loginUser,
	registerUser,
	updateUser,
	updateUserProfile,
} from '../../controllers/userControllers.js';

jest.useFakeTimers();

describe('Auth Controllers', () => {
	test('AuthController.login()', () => {
		const req = {
			body: {
				email: 'test@email.com',
				password: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const login = loginUser(req, res, next);
		expect(login).toBeDefined();
	});

	test('AuthController.registerUser', () => {
		const req = {
			body: {
				username: 'test',
				email: 'test@email.com',
				password: 'test',
				role: 'user',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const register = registerUser(req, res, next);
		expect(register).toBeDefined();
	});

	test('AuthController.getUserProfile', () => {
		const req = {
			body: {
				user: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const getUserPr = getUserProfile(req, res, next);
		expect(getUserPr).toBeDefined();
	});

	test('AuthController.updateUserProfile', () => {
		const req = {
			body: {
				user: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const updateUserPro = updateUserProfile(req, res, next);
		expect(updateUserPro).toBeDefined();
	});

	test('AuthController.getUsers', async () => {
		const req = {
			body: {
				users: ['test', 'test2', 'test3'],
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const getUs = getUsers(req, res, next);
		expect(getUs).toBeDefined();
	});

	test('AuthController.deleteUser', async () => {
		const req = {
			body: {
				user: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const deleteU = deleteUser(req, res, next);
		expect(deleteU).toBeDefined();
	});

	test('AuthController.getUserById', async () => {
		const req = {
			body: {
				user: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const getUId = getUserById(req, res, next);

		expect(getUId).toBeDefined();
	});

	test('AuthController.updateUser', async () => {
		const req = {
			body: {
				user: 'test',
			},
		};
		const res = {
			status: () => {
				return {
					send: () => {},
				};
			},
		};
		const next = () => {};
		const updateU = updateUser(req, res, next);
		expect(updateU).toBeDefined();
	});
});
