import express from 'express';
const router = express.Router();

import {
	getAllBusiness,
	getBusiness,
	createBusiness,
	updateBusiness,
	deleteBusiness,
	createBusinessReview,
	getTopBusiness,
} from '../controllers/businessControllers.js';

import { protect, admin } from '../middleware/authMiddleware.js';

//CRUD BUSINESS

router.get('/', getAllBusiness);
router.get('/top', getTopBusiness);
router.get('/:id', getBusiness);
router.post('/create', protect, createBusiness);
router.post('/create/:id/reviews', protect, createBusinessReview);
router.put('/update/:id', protect, admin, updateBusiness);
router.delete('/delete/:id', protect, admin, deleteBusiness);

export default router;
