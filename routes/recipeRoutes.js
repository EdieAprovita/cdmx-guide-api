import express from 'express';
const router = express.Router();

import {
	getAllRecipes,
	getRecipe,
	createRecipe,
	updateRecipe,
	deleteRecipe,
	getTopRecipes,
	createRecipeReview,
} from '../controllers/recipesControllers.js';

import { protect, admin } from '../middleware/authMiddleware.js';

//CRUD RECIPES

router.get('/', getAllRecipes);
router.get('/:id', getRecipe);
router.get('/top', getTopRecipes);
router.post('/create', protect, createRecipe);
router.post('/:id/reviews', protect, createRecipeReview);
router.put('/edit/:id', protect, admin, updateRecipe);
router.delete('/delete/:id', protect, admin, deleteRecipe);

export default router;
