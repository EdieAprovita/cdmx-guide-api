import express from 'express';
const router = express.Router();

import {
	getAllMarkets,
	getMarket,
	createMarket,
	updateMarket,
	deleteMarket,
	createMarketReview,
	getTopMarkets,
} from '../controllers/marketsControllers.js';

import { protect, admin } from '../middleware/authMiddleware.js';

//CRUD MARKETS

router.get('/', getAllMarkets);
router.get('/:id', getMarket);
router.get('/top', getTopMarkets);
router.post('/create/:id/reviews', protect, createMarketReview);
router.post('/create', protect, createMarket);
router.put('/update/:id', protect, admin, updateMarket);
router.delete('/delete/:id', protect, admin, deleteMarket);

export default router;
