import express from 'express';
const router = express.Router();

import {
	getAllProfessions,
	getProfession,
	createProfession,
	updateProfession,
	deleteProfession,
	createProfessionReview,
	getTopProfession,
} from '../controllers/professionControllers.js';

import { protect, professional, admin } from '../middleware/authMiddleware.js';

//CRUD PROFESSION

router.get('/', getAllProfessions);
router.get('/top', getTopProfession);
router.get('/:id', getProfession);
router.post('/create', protect, professional, createProfession);
router.post('/create/:id/reviews', protect, createProfessionReview);
router.put('/update/:id', protect, professional, updateProfession);
router.delete('/delete/:id', protect, admin, deleteProfession);

export default router;
