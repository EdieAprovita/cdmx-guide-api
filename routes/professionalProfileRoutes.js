import express from 'express';
const router = express.Router();

import {
	createUpdateProfile,
	deleteProfile,
	getAllProfiles,
	getProfile,
} from '../controllers/professionalProfileControllers.js';

import { admin, protect } from '../middleware/authMiddleware.js';

//CRUD PROFILES

router.get('/', getAllProfiles);
router.get('/:user_id', getProfile);
router.post('/edit/:user_id', protect, createUpdateProfile);
router.delete('/delete/:user_id', protect, admin, deleteProfile);

export default router;
