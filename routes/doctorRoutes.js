import express from 'express';
const router = express.Router();

import {
	getAllDoctors,
	getDoctor,
	createDoctor,
	updateDoctor,
	deleteDoctor,
	createDoctorReview,
	getTopDoctor,
} from '../controllers/doctorsControllers.js';

import { protect, admin } from '../middleware/authMiddleware.js';

//CRUD DOCTOR

router.get('/', getAllDoctors);
router.get('/top', getTopDoctor);
router.get('/:id', getDoctor);
router.post('/create', protect, createDoctor);
router.post('/create/:id/reviews', protect, createDoctorReview);
router.put('/update/:id', protect, admin, updateDoctor);
router.delete('/delete/:id', protect, admin, deleteDoctor);

export default router;
