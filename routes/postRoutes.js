import express from 'express';
const router = express.Router();

import {
	createPost,
	getAllPosts,
	getPostById,
	deletePost,
	commentOnPost,
	deleteComment,
} from '../controllers/postControllers.js';

import { protect, admin } from '../middleware/authMiddleware.js';

//CRUD POSTS

router.get('/', protect, getAllPosts);
router.get('/:id', protect, getPostById);
router.post('/create/:id', protect, createPost);
router.put('/comment/:id', protect, commentOnPost);
router.delete('/delete/:id', protect, admin, deletePost);
router.delete('/comment/:id/:comment_id', protect, admin, deleteComment);

export default router;
