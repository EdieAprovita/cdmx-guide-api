import authRouter from './authRoutes.js';
import businessRoutes from './businessRoutes.js';
import doctorRoutes from './doctorRoutes.js';
import marketRoutes from './marketsRoutes.js';
import postRoutes from './postRoutes.js';
import professionalProfileRoutes from './professionalProfileRoutes.js';
import professionRoutes from './professionRoutes.js';
import recipeRoutes from './recipeRoutes.js';
import restaurantsRoutes from './restaurantsRoutes.js';
import uploadRoutes from './uploadRoutes.js';

export {
	authRouter,
	marketRoutes,
	doctorRoutes,
	businessRoutes,
	postRoutes,
	professionRoutes,
	recipeRoutes,
	restaurantsRoutes,
	professionalProfileRoutes,
	uploadRoutes,
};
