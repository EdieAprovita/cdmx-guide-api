import express from 'express';
const authRouter = express.Router();

import {
	loginUser,
	registerUser,
	getUserProfile,
	updateUserProfile,
	getUsers,
	deleteUser,
	getUserById,
	updateUser,
} from '../controllers/userControllers.js';

import { protect, admin } from '../middleware/authMiddleware.js';

//AUTH ROUTES

authRouter.get('/users', protect, admin, getUsers);
authRouter.get('/me', protect, getUserProfile);
authRouter.get('/user/:id', protect, admin, getUserById);
authRouter.post('/register', registerUser);
authRouter.post('/login', loginUser);
authRouter.put('/update/me', protect, updateUserProfile);
authRouter.put('/update/:id', protect, admin, updateUser);
authRouter.delete('/delete/:id', protect, admin, deleteUser);

export default authRouter;
