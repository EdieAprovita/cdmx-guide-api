import express from 'express';
const router = express.Router();

import {
	getAllRestaurants,
	getRestaurant,
	createRestaurant,
	updateRestaurant,
	deleteRestaurant,
	getTopRestaurants,
	createRestaurantReview,
} from '../controllers/restaurantsControllers.js';

import { protect, admin } from '../middleware/authMiddleware.js';

//CRUD RESTAURANTS

router.get('/', getAllRestaurants);
router.get('/:id', getRestaurant);
router.get('/top', getTopRestaurants);
router.post('/:id/reviews', protect, createRestaurantReview);
router.post('/create', protect, createRestaurant);
router.put('/edit/:id', protect, admin, updateRestaurant);
router.delete('/delete/:id', protect, admin, deleteRestaurant);

export default router;
